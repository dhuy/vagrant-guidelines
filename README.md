# Guia para uso do Vagrant
---
### Chocolatey
Chocolatey é um gerenciador de pacotes, algo como o apt-get, mas desenvolvido para Windows.
```sh
$ iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
```
> Executar o comando em um Powershell aberto como Administrador

### VirtualBox e Vagrant 
VirtualBox é um software de virtualização desenvolvido visa criar ambientes para instalação de sistemas distintos.  
Vagrant é um software que cria e configura ambientes virtuais para desenvolvimento.
```sh
$ cinst virtualbox vagrant
```
> Executar o comando em um Powershell aberto como Administrador

### Cygwin
Cygwin é uma coleção de ferramentas GNU e Open Source que fornecem funcionalidades similares as ferramentas utilizadas em distribuições Linux.
```sh
$ cinst install cyg-get
```
> Executar o comando em um Powershell aberto como Administrador

### Instalando pacotes Cygwin via Chocolatey 
```sh
$ cyg-get openssh
$ cyg-get rsync
$ cyg-get ncurses
```
> Executar o comando em um Powershell aberto como Administrador

### Checando a instalação do Vagrant e do VBOXManage
```sh
$ vagrant -v
$ vboxmanage -v
```

### Iniciando a VM
Acesse a home:
```sh
$ cd /cygdrive/c/tools/cygwin/home/<user>
```
Crie uma pasta para a sua VM:
```sh
$ mkdir VM && cd VM
```
Inicie o Vagrant:
```sh
$ vagrant init hashcorp/precise64
```
Acesse a VM via ssh:
```sh
$ vagrant ssh
```

### Comandos importantes
Listar VMs que estão executando:
```sh
$ vboxmanage list runningvms
```
Caso haja edição do Vagrantfile há a necessidade de recarregar a VM:
```sh
$ vagrant reload
```
Iniciar uma VM (comando deve ser executado na mesma pasta do arquivo Vagrantfile):
```sh
$ vagrant up
```
Desligar uma VM (comando deve ser executado na mesma pasta do arquivo Vagrantfile):
```sh
$ vagrant halt
```
Suspender uma VM (comando deve ser executado na mesma pasta do arquivo Vagrantfile):
```sh
$ vagrant suspend
```
Resumir uma VM (comando deve ser executado na mesma pasta do arquivo Vagrantfile):
```sh
$ vagrant resume
```
Destruir uma VM (comando deve ser executado na mesma pasta do arquivo Vagrantfile):
```sh
$ vagrant destroy
```